set --universal fish_greeting
if status is-interactive
    colorscript -e 20
end

### PATH ###
set PATH $PATH /home/teedee/.local/bin
export PATH
function ll
    ls -lh $argv
end

### EXPORT ###
set TERM "xterm-256color" # Sets the terminal type

# Function for creating a backup file
# ex: backup file.txt
# result: copies file as file.txt.bak
function backup --argument filename
    cp $filename $filename.bak
end

# navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'

# Make short update and remove
alias suatup='sudo apt update && sudo apt -y full-upgrade'
alias suatrm='sudo apt autoremove -y && sudo apt-get clean --dry-run'

# Colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

### RANDOM COLOR SCRIPT ###
# Get this script from my GitLab: gitlab.com/dwt1/shell-color-scripts
alias cls='colorscript -e 20'

starship init fish | source
