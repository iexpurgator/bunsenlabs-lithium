#!/bin/bash

status=$(cmus-remote -Q | grep status | cut -d ' ' -f 2-)

if [[ $status = *"playing"* ]]; then
    echo "▮▮"
elif [[ $status = *"paused"* ]]; then
    echo "▶"
else
    echo "󰎈"
fi
