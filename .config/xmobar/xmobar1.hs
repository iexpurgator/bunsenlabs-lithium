-- xmobar config used by Vic Fryzel
-- Author: Vic Fryzel
-- https://github.com/vicfryzel/xmonad-config

-- If you're using a single display with a different resolution, adjust the
-- position argument below using the given calculation.
Config {
    font = "xft:MesloLGM Nerd Font-9",
    bgColor = "#1B2B34",
    fgColor = "#ebdbb2",
    lowerOnStart = False,
    overrideRedirect = False,
    allDesktops = True,
    persistent = True,
    commands = [
        Run MultiCpu ["-t","Cpu: <total0> <total1> <total2> <total3>","-L","30","-H","60","-h","#FFB6B0","-l","#CEFFAC","-n","#FFFFCC","-w","3"] 10,
        Run Memory ["-t","Ram: <used>M (<usedratio>%)","-H","8192","-L","4096","-h","#FFB6B0","-l","#CEFFAC","-n","#FFFFCC"] 10,
        Run Swap ["-t","Swap: <usedratio>%","-H","1024","-L","512","-h","#FFB6B0","-l","#CEFFAC","-n","#FFFFCC"] 10,
        Run Network "enp7s0" ["-t","E: <rx>kb, <tx>kb","-H","200","-L","10","-h","#FFB6B0","-l","#CEFFAC","-n","#B2B2FF"] 10,
        Run Network "wlp6s0" ["-t","W: <rx>kb, <tx>kb","-H","200","-L","10","-h","#FFB6B0","-l","#CEFFAC","-n","#B2B2FF"] 10,
        Run Date "(%a) %b - %d | %l:%M" "date" 10,
        Run Com "whoami" [] "" 10,
        Run Com "/bin/bash" ["-c", "~/.config/xmobar/get-volume.sh"]  "myvolume" 1,
        Run StdinReader
    ],
    sepChar = "%",
    alignSep = "}{",
    template = " %StdinReader%} {%multicpu% %memory% | %enp7s0% - %wlp6s0% | %myvolume% : <fc=#FABD2F>%date%</fc> : <fc=#CC241D>%whoami%</fc> "
}
