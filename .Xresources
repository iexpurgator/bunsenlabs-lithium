!-------- XTerm Terminal Settings {{{
!------------------------------------------------------

! xrdb -load ~/.Xresources

Xft.dpi: 96
Xft.antialias: true
Xft.hinting: true
Xft.rgba: rgb
Xft.autohint: true
Xft.hintstyle: hintfull
Xft.lcdfilter: lcdfilter

! Allow XTerm to report the TERM variable correctly.
! Do not set the TERM variable from your ~/.bashrc or ~/.bash_profile or similar file.
XTerm.termName: xterm-256color

! set font and fontsize
!XTerm*faceName: DroidSansMono Nerd Font
!XTerm*faceName: MesloLGM Nerd Font Mono:style=Regular:px=20:antialias=true
!XTerm*faceName: CaskaydiaCove Nerd Font Mono:style=Book:px=20:antialias=false
XTerm*geometry: 110x35

XTerm*renderFont: true
XTerm*faceName: xft:mononoki Nerd Font, \
                xft:JoyPixels:size=13, \
                xft:Monospace:style=Medium:size=13   
XTerm*faceSize: 11
XTerm*utf8: 2
XTerm*locale: true

URxvt*imLocale: en_US.UTF-8
URxvt*termName: rxvt-unicode-256color
URxvt*buffered: false
URxvt.intensityStyles: false
URxvt.font: xft:mononoki Nerd Font:size=12, \
            xft:JoyPixels:size=12, \
            xft:Monospace:style=Medium:size=12   
URxvt.saveLines: 2000
URxvt.scrollBar: false
URxvt.cursorColor: white

! VT Font Menu: Unreadable
XTerm*faceSize1: 8
! VT font menu: Tiny
XTerm*faceSize2: 10
! VT font menu: Medium
XTerm*faceSize3: 12
! VT font menu: Large
XTerm*faceSize4: 16
! VT font menu: Huge
XTerm*faceSize5: 22

! Ensure that your locale is set up for UTF-8.
XTerm.vt100.locale: true

! Cursor ====================================================
! pointer and cursor (blinking and color)
!XTerm*pointerColor: white
!XTerm*pointerColorBackground: black
!XTerm*cursorColor: white
XTerm*cursorBlink: true


!! Selecting Text ========================================================
! Only select text
XTerm*highlightSelection: true
! Remove trailing spaces
XTerm*trimSelection: true

!! Scrolling ========================================================
! Use: Shift-Pageup / Shift-Pagedown to scroll or mousewheel
! Lines of output that you can scroll back over
XTerm*saveLines: 16384

! Turn the scrollbar on, and put it on the right
! XTerm.vt100.scrollBar: true
! XTerm.vt100.scrollbar.width: 8
! XTerm*scrollBar: true
! XTerm*rightScrollBar: true

! Do not scroll when there is new input e.g. tail -f /var/syslog
XTerm*scrollTtyOutput: false


!! Keybinding ========================================================
! http://blog.rot13.org/2010/03/change-font-size-in-XTerm-using-keyboard.html
! - change fontsize on the fly (ctrl+plus = increase ; ctrl+minus = decrease, ctrl+0 = default)
! - copy/paste hotkey (ctrl+shift+c = copy ; ctrl+shift+v = paste)
! - open url (clickable links)
!   1) double click to highlight the full url
!   2) Shift + click it to open it
XTerm.vt100.translations: #override \n\
  Ctrl <Key> <: smaller-vt-font() \n\
  Ctrl <Key> >: larger-vt-font() \n\
  Ctrl <Key> 0: set-vt-font(d) \n\
  Ctrl Shift <Key>C: copy-selection(CLIPBOARD) \n\
  Ctrl Shift <Key>V: insert-selection(CLIPBOARD) \n\
  Shift <Btn1Up>: exec-formatted("xdg-open '%t'", PRIMARY) \n\
  <Btn1Up>: select-end(PRIMARY, CLIPBOARD, CUT_BUFFER0) \n\
  <Btn2Up>: insert-selection(PRIMARY)

! enable copy/paste hotkey to work (shift+insert = paste ; mouse highlight = copy)
XTerm*selectToClipboard: true
! disable fullscreen hotkey alt+enter (hotkey conflicts with weechat, midnight commander ...etc)
XTerm*fullscreen: never
! enable alt key to work
XTerm*metaSendsEscape: true
! Fix the backspace key (for Emacs)
XTerm.vt100.backarrowKey: false
XTerm.ttyModes: erase ^?
XTerm*allowWindowOps: true
XTerm*charClass: 33:48,36-47:48,58-59:48,61:48,63-64:48,95:48,126:48
XTerm*on3Clicks: regex ([[:alpha:]]+://)?([[:alnum:]!#+,./=?@_~-]|(%[[:xdigit:]][[:xdigit:]]))+

!####################
!###### Themes ######
!####################
! http://web.archive.org/web/20090130061234/http://phraktured.net/terminal-colors/

!-------- Theme: Customize {{{
! Base16 OceanicNext
! Scheme: https://github.com/voronianski/oceanic-next-color-scheme

#define base00 #1B2B34
#define base01 #343D46
#define base02 #4F5B66
#define base03 #65737E
#define base04 #A7ADBA
#define base05 #C0C5CE
#define base06 #CDD3DE
#define base07 #D8DEE9
#define base08 #EC5f67
#define base09 #F99157
#define base0A #FAC863
#define base0B #99C794
#define base0C #5FB3B3
#define base0D #6699CC
#define base0E #C594C5
#define base0F #AB7967

*.foreground:   base05
#ifdef background_opacity
*.background:   [background_opacity]base00
#else
*.background:   base00
#endif
*.cursorColor:  base05

*.color0:       base00
*.color1:       base08
*.color2:       base0B
*.color3:       base0A
*.color4:       base0D
*.color5:       base0E
*.color6:       base0C
*.color7:       base05

*.color8:       base03
*.color9:       base08
*.color10:      base0B
*.color11:      base0A
*.color12:      base0D
*.color13:      base0E
*.color14:      base0C
*.color15:      base07

! Note: colors beyond 15 might not be loaded (e.g., XTerm, urxvt),
! use 'shell' template to set these if necessary
*.color16:      base09
*.color17:      base0F
*.color18:      base01
*.color19:      base02
*.color20:      base04
*.color21:      base06
